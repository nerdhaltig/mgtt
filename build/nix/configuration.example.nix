# nix-instantiate '<nixpkgs/nixos>' -I nixos-config=./configuration.example.nix -A system
# nix-build '<nixpkgs/nixos>' -I nixos-config=./configuration.example.nix -A system
{ config, lib, pkgs, modulesPath, ... }:
let

  # For local testing
  # mgttPackage = import ../../default.nix { };

  mgttPackage = (import <mgtt> { });

in
{

  #imports = [
  # For local testing
  #(import ../../default.nix { }).module
  #];

  # tag::import[]
  imports = [
    (import <mgtt> { }).module
  ];
  # end::import[]

  boot.loader.grub.device = "/dev/sda";
  fileSystems."/".device = "/dev/sda1";

  # tag::service[]
  services.mgtt = {
    enable = true;


  };

  # end::service[]

}
