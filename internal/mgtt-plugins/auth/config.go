package auth

import (
	"encoding/base64"

	"golang.org/x/crypto/bcrypt"
)

type configStruct struct {
	Enable bool `yaml:"enable"`
	Anonym bool `yaml:"anonym"`

	New   []configUserStruct          `yaml:"new,omitempty"`
	Users map[string]configUserStruct `yaml:"users,omitempty"`
}

// userSet will set passwords/group for an user and return the new user-Object
//
// if the user already exist, we override the password
func (config *configStruct) SetUser(username string, password *string, groups *[]string) (user configUserStruct, err error) {

	// get user
	user, _ = config.GetUser(username)

	// you can not set the anonymouse-password
	if username == "" {
		return
	}

	// password
	if password != nil {
		var bcryptedData []byte
		bcryptedData, err = bcrypt.GenerateFromPassword([]byte(*password), bcrypt.DefaultCost)

		user.Password = base64.StdEncoding.EncodeToString(bcryptedData)
	}

	// groups
	if groups != nil {
		user.Groups = *groups
	}

	// save it to the config
	config.Users[username] = user

	// get user
	user, _ = config.GetUser(username)
	return
}

// GetUser return the user without password
func (config *configStruct) GetUser(username string) (user configUserStruct, exist bool) {

	if username == "" {
		user.Username = "_anonym"
		user.Password = ""

		exist = true
		return
	}

	if user, exist = config.Users[username]; exist {
		user.Username = username
		user.Password = ""
	}
	return
}

// CheckPassword will check if a password match
func (config *configStruct) CheckPassword(username, password string) (match bool) {

	// if we allow anonym access the anonym-user ( "" ) is allowed
	if config.Anonym && username == "" {
		return true
	}

	if currentUser, userExist := config.Users[username]; userExist {
		match = currentUser.PasswordCheck(password)
	}

	return
}
