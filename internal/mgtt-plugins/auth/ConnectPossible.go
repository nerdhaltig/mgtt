package auth

import (
	"encoding/base64"

	"golang.org/x/crypto/bcrypt"
)

// configCheckPassword
func (plugin *Plugin) ConnectPossible(clientID string, username string, password string) (isOkay bool) {

	if plugin.config.Anonym && username == "" {
		return true
	}

	// get user
	var user = plugin.config.Users[username]

	basswordBytes, err := base64.StdEncoding.DecodeString(user.Password)
	if err == nil {
		errCompare := bcrypt.CompareHashAndPassword(basswordBytes, []byte(password))
		if errCompare == nil {
			isOkay = true
		}
	}

	return
}
