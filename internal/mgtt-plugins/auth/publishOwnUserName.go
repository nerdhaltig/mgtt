package auth

import (
	"encoding/json"

	"github.com/rs/zerolog/log"
)

// Not working and not used
func (plugin *Plugin) publishOwnUserName(originClientID string) {
	var err error
	//var username = clientlist.Get(originClientID).Username()
	var username = ""
	// check if the user exist
	if user, exist := plugin.config.GetUser(username); exist {

		// create a json and send it
		var jsonData []byte
		jsonData, err = json.Marshal(user)
		if err == nil {

			err = plugin.broker.PublishToClient(
				originClientID,
				"$SYS/self/user/json",
				jsonData,
				false,
				0,
			)

		}

	} else {
		err = plugin.broker.PublishToClient(
			originClientID,
			"$SYS/self/user/error",
			[]byte("User dont exist"),
			false,
			0,
		)
	}

	if err != nil {
		log.Error().Err(err).Send()
	}
}
