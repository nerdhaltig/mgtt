package acl

import "github.com/rs/zerolog/log"

// OnSendToSubscriberRequest get called when the broker try to publish a message to an subscriber
func (plugin Plugin) PublishToSubscriberPossible(clientID string, username string, publishTopic string) (accepted bool) {

	// check global permission
	allowedGlobally := plugin.checkACL(clientID, "_global", publishTopic, "r")

	// check for specific username
	accepted = plugin.checkACL(clientID, username, publishTopic, "r") || allowedGlobally

	if !accepted {
		log.Warn().Str("topic", publishTopic).Msg("Not allowed")
	}

	return accepted
}
