package acl

type pluginConfigStruct struct {
	Enable bool                  `yaml:"enable"`
	Rules  map[string][]aclEntry `yaml:"rules"`
}
