package acl

import "gitlab.com/mgtt/internal/mgtt/plugin"

type aclEntry struct {
	Route string `yaml:"route"`

	// "r" / "w"
	Direction string `yaml:"direction"`

	// true if allow, false if not
	Allow bool `yaml:"allow"`
}

type Plugin struct {
	broker plugin.Broker
	config pluginConfigStruct
}

func Create(broker plugin.Broker) (plugin Plugin) {
	ValidateFeatures(&plugin)

	// store broker
	plugin.broker = broker

	// init the map
	plugin.config.Rules = make(map[string][]aclEntry)

	return
}

func (plugin Plugin) Name() string {
	return "acl"
}
