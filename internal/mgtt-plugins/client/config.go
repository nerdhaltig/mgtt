package client

type configStruct struct {
	Enable  bool     `yaml:"enable"`
	Remotes []remote `yaml:"remotes"`
}

type remote struct {
	URL string `yaml:"url"`
}
