package broker

import (
	"github.com/eclipse/paho.mqtt.golang/packets"
	"gitlab.com/mgtt/internal/mgtt/client"
)

func (broker *Broker) onPacketConACK(connectedClient *client.MgttClient, packet *packets.ConnackPacket) (err error) {
	connectedClient.Connected = true

	return
}
