package broker

// UserNameOfClient return the username of an client
func (broker *Broker) UserNameOfClient(clientID string) (username string) {

	// find the client
	client := broker.clients.Get(clientID)
	if client != nil {
		username = client.Username()
	}

	return
}
