package broker

// ServeClose will close all client-connections and broker-listeners
func (b *Broker) ServeClose() {

	// b.loopHandleResendPacketsExit <- true
	b.clients.RemoveAll()
	b.serverListener.Close()

}
