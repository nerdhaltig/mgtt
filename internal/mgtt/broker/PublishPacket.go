package broker

import (
	"github.com/eclipse/paho.mqtt.golang/packets"
)

// PublishPacket publish a packet to all subscribers
//
// err will return the last occured error of an subscriber
func (broker *Broker) PublishPacket(sourceClientID string, packet *packets.PublishPacket, once bool) (messagedelivered bool, subscribed bool, err error) {

	// [MQTT-3.3.1-9]
	// MUST set the RETAIN flag to 0 when a PUBLISH Packet is sent to a Client
	// because it matches an established subscription
	packet.Retain = false

	messagedelivered, subscribed, err = broker.clients.PublishToAllClients(packet, sourceClientID, once, broker.plugins.PublishToSubscriberPossible)

	return
}
