package broker

import (
	"github.com/eclipse/paho.mqtt.golang/packets"
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/client"
	"gitlab.com/mgtt/internal/mgtt/persistance"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

func (broker *Broker) onPacketConnect(connectedClient *client.MgttClient, packet *packets.ConnectPacket) (err error) {

	// MQTT-3.1.0-2
	// Check if the client is already connected
	if exists := broker.clients.Exist(packet.ClientIdentifier); exists {
		utils.PanicWithString("Protocol violation. Client already exist")
	}

	// PLUGINS: check if plugins accept the client
	accepted := broker.plugins.ConnectPossible(connectedClient.ID(), packet.Username, string(packet.Password))
	if !accepted {
		err = connectedClient.SendConnack(client.ConnackUnauthorized, false)
		utils.PanicOnErr(err)
		utils.PanicWithString("Client not accepted by plugin")
	}

	// add client to the list

	// Move the client to the newID, but only if its not empty
	if packet.ClientIdentifier != "" {
		err = broker.clients.Move(connectedClient.ID(), packet.ClientIdentifier)
		utils.PanicOnErr(err)
	} else {
		log.Warn().Str("cid", connectedClient.ID()).Msg("ClientIdentifier is empty, so we use an random one.")
	}

	// store the username
	connectedClient.UsernameSet(packet.Username)

	// set the client to connected so that the broker will accept other packets from it
	connectedClient.Connected = true

	// reset timeout
	connectedClient.ResetTimeout()
	connectedClient.KeepAliveSetTimeout(packet.Keepalive)

	// MQTT-3.1.2-9
	// Las will message ?
	if packet.WillFlag {
		// we create a new publish packet
		pubPacket := packets.NewControlPacket(packets.Publish).(*packets.PublishPacket)
		pubPacket.Retain = packet.WillRetain
		pubPacket.TopicName = packet.WillTopic
		pubPacket.Payload = packet.WillMessage
		pubPacket.Qos = packet.WillQos

		connectedClient.LastWillSet(pubPacket)
	}

	// [MQTT-3.1.2-6] If CleanSession is set to 1, the Client and Server MUST discard any previous Session and start a new one.
	// This Session lasts as long as the Network Connection.
	// State data associated with this Session MUST NOT be reused in any subsequent Session.
	var sessionExist bool = false

	if packet.CleanSession {
		connectedClient.CleanSessionSet(true)
		persistance.CleanSession(connectedClient.ID())
	} else {
		sessionSubscriptions := persistance.SubscriptionsGet(connectedClient.ID())
		connectedClient.SubScriptionsAdd(sessionSubscriptions)
		if len(sessionSubscriptions) > 0 {
			sessionExist = true
		}
	}

	// [MQTT-3.2.2-2]  If the Server accepts a connection with CleanSession set to 0, the value set in Session Present depends on
	// whether the Server already has stored Session state for the supplied client ID.
	// If the Server has stored Session state, it MUST set Session Present to 1 in the CONNACK packet.
	err = connectedClient.SendConnack(client.ConnackAccepted, sessionExist)
	utils.PanicOnErr(err)

	// PLUGINS: inform all plugins that a new client is there
	broker.plugins.SendConnected(connectedClient.ID())

	return
}
