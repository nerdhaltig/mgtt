package clientlist

import (
	"github.com/eclipse/paho.mqtt.golang/packets"
)

// PublishToAllClients send an packet to all clients
func (l *List) PublishToAllClients(
	packet *packets.PublishPacket, skipClientID string, once bool,
	CanSendToSubscriber func(clientID string, username string, publishTopic string) (accepted bool),
) (published bool, subscribed bool, err error) {

	if CanSendToSubscriber == nil {
		CanSendToSubscriber = func(clientID string, username string, publishTopic string) (accepted bool) { return true }
	}

	// mutex
	l.writeLock.Lock()
	defer l.writeLock.Unlock()

	for _, client := range l.list {

		clientID := client.ID()
		userName := client.Username()

		if clientID == skipClientID {
			continue
		}

		if CanSendToSubscriber(clientID, userName, packet.TopicName) {

			publishOk, subscriptionOK, publishErr := client.Publish(packet)
			if once {
				if publishOk {
					return true, true, nil
				}
			}

			published = published || publishOk
			subscribed = subscribed || subscriptionOK

			if publishErr != nil && err == nil {
				err = publishErr
			}

		}

	}

	return
}
