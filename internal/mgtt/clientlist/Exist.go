package clientlist

// Exist return true if clientID exist in the list
func (l *List) Exist(clientID string) (exist bool) {
	_, exist = l.list[clientID]
	return
}
