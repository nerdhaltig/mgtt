package clientlist

// RemoveAll will remove and disconnects all clients
func (l *List) RemoveAll() {

	// find the client
	l.writeLock.Lock()
	defer l.writeLock.Unlock()

	// prevent "fatal error: concurrent map iteration and map write"
	var keys []string

	// close all clients
	for clientID, client := range l.list {
		client.Close()
		keys = append(keys, clientID)
	}

	// Delete all clints
	for _, key := range keys {
		delete(l.list, key)
	}

}
