package clientlist

import (
	"errors"

	"github.com/rs/zerolog/log"
)

func (l *List) Add(existingClient Client) (err error) {

	// mutex
	l.writeLock.Lock()
	defer l.writeLock.Unlock()

	if _, clientExist := l.list[existingClient.ID()]; clientExist {
		err = errors.New("Client already exist")
	} else {
		l.list[existingClient.ID()] = existingClient
		log.Debug().Str("client", existingClient.ID()).Msg("Client added")
	}

	return
}
