package clientlist

import "errors"

// Move will move an client to an new ID
//
// The client ID will also be set for the new client
func (l *List) Move(oldID, newID string) (err error) {
	// mutex
	l.writeLock.Lock()
	defer l.writeLock.Unlock()

	if currentClient, exist := l.list[oldID]; exist {

		// remove the client
		delete(l.list, oldID)

		// set an new id
		currentClient.IDSet(newID)

		// and re-add it to the list if possible
		l.list[currentClient.ID()] = currentClient
	} else {
		err = errors.New("client don't exist")
	}

	return
}
