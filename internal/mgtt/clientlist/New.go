package clientlist

import "sync"

type List struct {
	writeLock sync.Mutex
	list      map[string]Client
}

func Create() (newList List) {
	newList.list = make(map[string]Client)
	return
}
