package pluginlist

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/plugin"
)

// PublishToBrokerPossible ask all plugins if the message is allowed to publish to the broker
//
// calls OnPublishRequest on all plugins
func (s Store) PublishToBrokerPossible(clientID string, username string, topic string) (accepted bool) {

	//per default we should accept
	accepted = true

	for pluginName, currentPluginInterface := range s {

		if currentPlugin, ok := currentPluginInterface.(plugin.V2MessageFilter); ok {
			log.Debug().Str("plugin", pluginName).Msg("call OnAcceptNewClient")
			pluginAccept := accepted && currentPlugin.PublishToBrokerPossible(clientID, username, topic)

			if !pluginAccept {
				accepted = false

				log.Warn().
					Str("cid", clientID).
					Str("plugin", pluginName).
					Str("topic", topic).
					Msg("plugin not accept send to broker")
			}

		}
	}

	return
}
