package pluginlist

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/plugin"
)

type Store map[string]plugin.V2

func Create() (newStore Store) {
	return make(Store)
}

// Register will register a new Plugin
func (s Store) Register(newPlugin plugin.V2) {
	log.Info().Str("name", newPlugin.Name()).Msg("Registered new plugin")
	s[newPlugin.Name()] = newPlugin
}

// DeRegister will remove an plugin from the pluginlist
func (s Store) DeRegister(name string) {
	log.Info().Str("name", name).Msg("DeRegister plugin")
	delete(s, name)
}
