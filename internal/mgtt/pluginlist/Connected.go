package pluginlist

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/plugin"
)

// SendConnected inform all plugins that a new client is connected and accepted by all plugins
//
// calls OnConnected on all plugins
func (s Store) SendConnected(clientID string) {

	for pluginName, currentPluginInterface := range s {
		if currentPlugin, ok := currentPluginInterface.(plugin.V2ConnectNotify); ok {
			log.Debug().Str("plugin", pluginName).Msg("call OnConnected")
			currentPlugin.OnConnected(clientID)
		}
	}

}

// SendDisconnected inform all plugins that we disconnected the client
//
// calls OnDisconnected on all plugins
func (s Store) SendDisconnected(clientID string) {

	for pluginName, currentPluginInterface := range s {
		if currentPlugin, ok := currentPluginInterface.(plugin.V2ConnectNotify); ok {
			log.Debug().Str("plugin", pluginName).Msg("call OnDisconnected")
			currentPlugin.OnDisconnected(clientID)
		}
	}

}
