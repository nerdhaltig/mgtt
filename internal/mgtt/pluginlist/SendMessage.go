package pluginlist

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/plugin"
)

// SendMessage send a message to all plugins
//
// If this function return true, the plugin handled the message and no other plugin will get it
//
// If a plugin handle the message, it will NOT sended to subscribers
func (s Store) SendMessage(originClientID string, topic string, payload []byte) (messageWasHandled bool) {

	for pluginName, currentPluginInterface := range s {
		if currentPlugin, ok := currentPluginInterface.(plugin.V2MessageHandler); ok {
			log.Debug().Str("plugin", pluginName).Msg("call OnConnected")
			currentPlugin.HandleMessage(originClientID, topic, payload)
		}
	}

	return
}
