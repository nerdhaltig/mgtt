package pluginlist

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/plugin"
)

// SubscribePossible ask all plugins if its allowed to subscribe to this topic
//
// calls OnSubscriptionRequest on all plugins
func (s Store) SubscribePossible(clientID string, username string, subscriptionTopic string) (accepted bool) {

	//per default we should connect
	accepted = true

	for pluginName, currentPluginInterface := range s {

		if currentPlugin, ok := currentPluginInterface.(plugin.V2SubscriptionFilter); ok {
			log.Debug().Str("plugin", pluginName).Msg("call OnAcceptNewClient")
			accepted = accepted && currentPlugin.SubscriptionPossible(clientID, username, subscriptionTopic)
		}
	}

	return
}
