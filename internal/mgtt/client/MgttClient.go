package client

import (
	"net"
	"time"

	"github.com/eclipse/paho.mqtt.golang/packets"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"
)

// MgttClient represents a mqtt-client
type MgttClient struct {
	id           string
	username     string
	cleanSession bool
	connection   net.Conn
	Connected    bool

	// keep-alive
	// MQTT-3.1.2-24
	keepAliveTimeoutSeconds uint16    //
	keepAliveDeadline       time.Time //

	// The last will of this client
	lastWillPacket *packets.PublishPacket

	subscriptionTopics []string

	recvPackets <-chan packets.ControlPacket // recv buffer
	sendPackets chan packets.ControlPacket   // send-buffer to avoid double-write

	// loop signals
	commLoopRunning bool // indicates i the send-loop is running
	commLoopExit    chan bool
}

// Init create a new MgttClient with id of "unknown"
func (c *MgttClient) Init(connection net.Conn, timeout time.Duration) {

	// create a new client with an new random-id
	guid := xid.New()
	c.id = guid.String()

	c.connection = connection

	// keep alive
	c.keepAliveTimeoutSeconds = 30
	c.KeepAliveUpdate()

	// setup timeout
	if timeout > 0 {
		log.Debug().Dur("timeout", timeout).Msg("Set deadline for client")
		err := connection.SetDeadline(time.Now().Add(timeout))
		if err != nil {
			log.Error().Err(err).Send()
		}
	}

}

// ResetTimeout will disable the timeout
func (c *MgttClient) ResetTimeout() {

	err := c.connection.SetDeadline(time.Time{})
	if err != nil {
		log.Error().Err(err).Send()
	}
}

// IDSet set the clientID
func (c *MgttClient) IDSet(id string) {
	c.id = id
}

// ID return the id of an MgttClient
func (c *MgttClient) ID() string {
	return c.id
}
