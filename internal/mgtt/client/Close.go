package client

// Close will close an network connection
func (client *MgttClient) Close() (err error) {

	if client.connection == nil {
		return nil
	}

	// close network-connection
	err = client.connection.Close()
	client.connection = nil
	client.Connected = false
	return
}
