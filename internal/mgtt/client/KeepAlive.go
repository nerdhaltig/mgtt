package client

import (
	"time"

	"github.com/rs/zerolog/log"
)

// KeepAliveSetTimeout will set the keep alive timeout
//
// we multiply this time bi 1,5 according to MQTT-3.1.2-24
func (c *MgttClient) KeepAliveSetTimeout(timeout uint16) {
	c.keepAliveTimeoutSeconds = uint16(float32(timeout) * 1.5)

	log.Debug().
		EmbedObject(c).
		Uint16("Timeout", c.keepAliveTimeoutSeconds).
		Msg("set keep alive timeout")

	c.KeepAliveUpdate()
}

// KeepAliveUpdate will update the timestamp of last received message
//
// MQTT-3.1.2-24 If the Keep Alive value is non-zero and the Server does not receive a
// Control Packet from the Client within one and a half times the Keep Alive time period,
// it MUST disconnect the Network Connection to the Client as if the network had failed
func (c *MgttClient) KeepAliveUpdate() {

	c.keepAliveDeadline = time.Now().Add(
		time.Second * time.Duration(c.keepAliveTimeoutSeconds),
	)

}

// KeepAliveCheck return if keep alive timeouts
//
// return true if keep alive is over the deadline
func (c *MgttClient) KeepAliveCheck() bool {
	return time.Now().After(c.keepAliveDeadline)
}
