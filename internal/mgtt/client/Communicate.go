package client

import (
	"time"

	"github.com/eclipse/paho.mqtt.golang/packets"
	"github.com/rs/zerolog/log"
)

// Communicate start an loop ( non-blocking )
// that will handle incoming/sending packages and check keep alive
//
// the channels returned from this function, will be closed by this function ! Don't close it yourselfe
func (c *MgttClient) Communicate() (incomingPackages chan packets.ControlPacket, done chan bool) {

	if c.commLoopRunning {
		log.Error().Str("client", c.ID()).Msg("communication loop already running")
		return
	}

	// kommunication
	c.sendPackets = make(chan packets.ControlPacket, 10)
	c.commLoopExit = make(chan bool)
	incomingPackages = make(chan packets.ControlPacket)
	done = make(chan bool)

	// the read channel
	c.recvPackets = ReaderChannel(c.connection)

	// keepAliveTicker
	keepAliveTicker := time.NewTicker(2 * time.Second)

	c.commLoopRunning = true

	log.Debug().
		Str("client", c.ID()).
		Msg("kommunication started")

	go func() {

	loop:
		for {

			select {

			case packetReceived, ok := <-c.recvPackets:
				if !ok {
					log.Error().
						Str("client", c.ID()).
						Msg("received loop is closed")
					break loop
				}

				c.KeepAliveUpdate() // MQTT-3.1.2-24

				incomingPackages <- packetReceived

			case packetToSend, ok := <-c.sendPackets:
				if !ok {
					log.Error().
						Str("client", c.ID()).
						Msg("send loop is closed")
					break loop
				}

				// connected ?
				if c.connection == nil {
					log.Warn().
						Str("client", c.ID()).
						Msg("can not send packet, connection is closed")
					break loop
				}

				// send it
				err := packetToSend.Write(c.connection)
				if err != nil {
					log.Error().
						Str("client", c.ID()).
						Err(err).Send()
					break loop
				}

				log.Debug().
					Str("client", c.ID()).
					Str("packet", packetToSend.String()).
					Msg("packet send")

			// the keep alive timer
			// MQTT-3.1.2-24
			case <-keepAliveTicker.C:
				if c.KeepAliveCheck() {
					log.Error().
						Str("client", c.ID()).
						Msg("Keep alive timeout")

					break loop
				}

			// normal exit was commented
			case <-c.commLoopExit:
				log.Debug().
					Str("client", c.ID()).
					Msg("kommunication loop stopped normaly")

				break loop
			}

		}

		c.commLoopRunning = false
		log.Debug().Str("client", c.ID()).Msg("kommunication ended")
		done <- true

		close(done)
		close(incomingPackages)
	}()

	return
}
