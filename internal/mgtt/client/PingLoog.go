package client

import (
	"time"

	"github.com/rs/zerolog/log"
)

// PingLoop will start a loop which sends out a ping
//
// this function blocks, so you may will use go PingLoop() ;)
//
// KeepAliveSetTimeout() should be called before, the time can not be changed after
//
// if the connection to the client is closed, the loop will end
func (client *MgttClient) PingLoop() {

	if client.keepAliveTimeoutSeconds == 0 {
		log.Warn().Object("client", client).Msg("no timeout configured")
		return
	}

	pingResendSeconds := uint16(float32(client.keepAliveTimeoutSeconds) / 1.5)

	for {
		time.Sleep(time.Second * time.Duration(pingResendSeconds))
		client.SendPingreq()
		if client.connection == nil || !client.Connected {
			break
		}
	}

}
