module gitlab.com/mgtt

go 1.16

require (
	github.com/alecthomas/kong v0.6.1
	github.com/boltdb/bolt v1.3.1
	github.com/eclipse/paho.mqtt.golang v1.4.1
	github.com/google/uuid v1.3.0
	github.com/mcuadros/go-defaults v1.2.0
	github.com/rs/xid v1.4.0
	github.com/rs/zerolog v1.27.0
	github.com/stretchr/testify v1.7.2
	gitlab.com/stackshadow/qommunicator/v2 v2.5.1
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e
	golang.org/x/sys v0.0.0-20220615213510-4f61da869c0c // indirect
	gopkg.in/yaml.v2 v2.4.0
)
